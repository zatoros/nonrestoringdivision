using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DivisionAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("CHECK SCAN-----------------------");
            try
            {
                for (int n = 2; n < 14; n++)
                {
                    int min = -(1 << n - 1);
                    int max = -min - 1;
                    for (int a = min; a <= max; a++)
                        for (int b = min; b <= max; b++)
                            if (b != 0)
                                CheckScan(a, b, n);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("CHECK FINISHED-----------------------");
            try
            {
                while (true)
                {
                    string[] lines = Console.ReadLine().Split(' ');
                    int a, b, n;
                    a = int.Parse(lines[0]);
                    b = int.Parse(lines[1]);
                    n = int.Parse(lines[2]);
                    RunDivisionAlgorithm(a, b, n);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("PROGRAM FINISHED-----------------------");
            }
        }

        static int[] RunDivisionAlgorithm(int a, int b, int n, bool hideOutput = false)
        {
            int bp;
            bp = b << (n - 2);
            if (!hideOutput)
            {
                Console.WriteLine("INIT-----------------------");
                Console.WriteLine("A = {0}, B = {1}, B' = {2}, N = {3}", a, b, bp, n);
                Console.WriteLine("START-----------------------");
            }
            int r = a;
            BitArray q = new BitArray(n);
            if (!hideOutput)
            {
                Console.WriteLine("R = {0}", r);
                Console.WriteLine("----------------------------");
            }
            for (int i = 0; i < n - 1; i++)
            {
                if ((r >= 0 && b >= 0) || (r < 0 && b < 0)) //XNOR
                {

                    q[n - 1 - i] = true;
                    r = (r - bp) << 1;
                }
                else
                {

                    q[n - 1 - i] = false;
                    r = (r + bp) << 1;
                }
                if (!hideOutput)
                {
                    Console.WriteLine("STEP {0}-----------------------", i);
                    Console.WriteLine("R = {0}, Q = {1}", r, PrintBits(q));
                }
                Debug.Assert((r & 0x1) == 0);
            }
            if (!hideOutput)
            {
                Console.WriteLine("SIGN CORRECTION 1-----------------------");
            }
            r >>= (n - 1);
            q[0] = true;
            q[n - 1] = !q[n - 1];
            int qres = GetValue(q);
            if (!hideOutput)
            {
                Console.WriteLine("R = {0}, Q = {1} = {2}", r, PrintBits(q), qres);
                Console.WriteLine("SIGN CORRECTION 2-----------------------");
            }
            int mod = r;
            if ((b >= 0 && ((r > 0 && a < 0) || r >= b)) ||
                (b < 0 && ((r < 0 && a >= 0) || r <= b)))
            {
                r -= b;
                qres++;

            }
            else if ((b >= 0 && ((r < 0 && a >= 0) || r <= -b)) ||
                (b < 0 && ((r > 0 && a < 0) || r >= -b)))
            {
                r += b;
                qres--;
            }

            if ((b >= 0 && mod >= b) ||
                (b < 0 && mod <= b))
            {
                mod -= b;
            }
            else if ((b >= 0 && mod < 0) ||
                (b < 0 && mod > 0))
            {
                mod += b;
            }

            if (!hideOutput)
            {
                Console.WriteLine("R = {0}, Q = {1}, MOD = {2}", r, qres, mod);
            }

            return new int[] { qres, r, mod };
        }

        static string PrintBits(BitArray q)
        {
            string str = string.Empty;
            for (int i = q.Length - 1; i >= 0; i--)
                str += q[i] ? "1" : "0";
            return str;
        }

        static int GetValue(BitArray q)
        {
            int val = 0;
            for (int i = 0; i < q.Length; i++)
                if (i == q.Length - 1)
                    val -= q[i] ? (1 << i) : 0;
                else
                    val += q[i] ? (1 << i) : 0;
            return val;
        }

        static void CheckScan(int a, int b, int n)
        {
            int[] result = RunDivisionAlgorithm(a, b, n, true);
            int qexp = a / b;
            int rexp = a % b;
            if (qexp != result[0] || rexp != result[1] || !CheckMod(a, b, result[2]))
                throw new Exception(string.Format("Test failed: a = {0}, b = {1}, n = {2}, result: q = {3}, r = {4}, mod = {5}, expected q = {6}, r = {7}",
                    a, b, n, result[0], result[1], result[2], qexp, rexp));
        }

        static bool CheckMod(int a, int b, int mod)
        {
            return ((Math.Abs(mod) < Math.Abs(b)) && ((mod > 0 && b >= 0) || (mod < 0 && b < 0))) || mod == 0;
        }
    }
}
